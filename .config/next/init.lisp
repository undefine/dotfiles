(in-package :next)

;; Use development platform port.
(setf (get-default 'port 'path)
      "~/common-lisp/next/ports/gtk-webkit/next-gtk-webkit")

(defun old-reddit-hook (url)
  (let* ((uri (quri:uri url)))
    (if (search "www.reddit" (quri:uri-host uri))
        (progn
          (setf (quri:uri-host uri) "old.reddit.com")
          (let ((new-url (quri:render-uri uri)))
            (log:info "Switching to old Reddit: ~a" new-url)
            new-url))
        url)))
(add-to-default-list #'old-reddit-hook 'buffer 'load-hook)

(defun blocker-hook (url)
  (match (quri:uri-host (quri:uri url))
    ("www.imdb.com" (next/blocker-mode:blocker-mode :activate t)))
  url)
(add-to-default-list #'blocker-hook 'buffer 'load-hook)

(setf (get-default 'remote-interface 'download-directory)
      "~/temp")

(add-to-default-list 'vi-normal-mode 'buffer 'default-modes)
(add-to-default-list 'proxy-mode 'buffer 'default-modes)
(add-to-default-list 'blocker-mode 'buffer 'default-modes)

;; TODO: Fetch from Emacs' engine.el automatically?
(nconc (get-default 'window 'search-engines)
       '(("aa" . "https://aur.archlinux.org/packages.php?O=0&K=~a&do_Search=Go")
         ("ap" . "https://www.archlinux.org/packages/?sort=&q=~a&maintainer=&flagged=")
         ("aw" . "https://wiki.archlinux.org/index.php?search=~a")
         ("ctan" . "http://www.ctan.org/search?phrase=~a")
         ("dd" . "http://devdocs.io/#q=~a")
         ("dg" . "https://duckduckgo.com/?q=~a")
         ("eb" . "https://debbugs.gnu.org/cgi/pkgreport.cgi?package=emacs;include=subject%3A~a;repeatmerged=on;archive=both")
         ("ed" . "https://lists.gnu.org/archive/cgi-bin/namazu.cgi?idxname=emacs-devel&submit=Search&query=~a")
         ("ee" . "https://lists.gnu.org/archive/cgi-bin/namazu.cgi?idxname=emms-help&submit=Search!&query=~a")
         ("ge" . "https://wiki.gentoo.org/index.php?title=Special%3ASearch&search=~a&go=Go")
         ("gh" . "https://github.com/search?ref=simplesearch&q=~a")
         ("gr" . "https://www.goodreads.com/search?q=~a")
         ("gm" . "https://maps.google.com/maps?q=~a")
         ("gud" . "https://lists.gnu.org/archive/cgi-bin/namazu.cgi?idxname=guix-devel&submit=Search&query=~a")
         ("guh" . "https://lists.gnu.org/archive/cgi-bin/namazu.cgi?idxname=help-guix&submit=Search!&query=~a")
         ("gup" . "https://guix-hpc.bordeaux.inria.fr/package/~a")
         ("i" . "http://www.imdb.com/find?q=~a&s=all")
         ("mba" . "http://musicbrainz.org/search?query=~a&type=artist&method=indexed")
         ("mbr" . "http://musicbrainz.org/search?query=~a&type=release&method=indexed")
         ("osm" . "https://www.openstreetmap.org/search?query=~a")
         ("q" . "http://quickdocs.org/search?q=~a")
         ("s" . "http://stackoverflow.com/search?q=~a")
         ("wp" . "http://www.wikipedia.org/search-redirect.php?language=en&go=Go&search=~a")
         ("wb" . "http://en.wikibooks.org/wiki/Special:Search?search=~a")
         ("wk" . "http://en.wiktionary.org/wiki/Special:Search?search=~a")
         ("wa" . "http://www.winehq.org/search/?q=~a")
         ("yt" . "https://www.youtube.com/results?search_query=~a")))

(add-to-default-list
 (next/blocker-mode:make-hostlist
  :hosts '("platform.twitter.com"
           "syndication.twitter.com"
           "m.media-amazon.com"))
 'next/blocker-mode:blocker-mode 'next/blocker-mode:hostlists)

(defun eval-in-emacs (&rest s-exps)
  "Evaluate S-exps with `emacsclient'."
  (let ((s-exps-string (str:replace-all
                        ;; Discard the package prefix.
                        "next::" ""
                        (write-to-string
                         `(progn ,@s-exps) :case :downcase))))
    (log:debug "Sending to Emacs: ~a" s-exps-string)
    (ignore-errors (uiop:run-program
                    (list "emacsclient" "--eval" s-exps-string)))))

(define-command org-capture ()
  "Org-capture current page."
  (with-result* ((url (buffer-get-url))
                 (title (buffer-get-title)))
    (eval-in-emacs
     `(org-link-set-parameters
       "next"
       :store (lambda ()
                (org-store-link-props
                 :type "next"
                 :link ,url
                 :description ,title)))
     `(org-capture))))

(define-key "C-M-o" #'org-capture)
(define-key :scheme :vi-normal
  "C-M-o" #'org-capture)

(define-command youtube-dl-current-page ()
  "Download a video in the currently open buffer."
  (with-result (url (buffer-get-url))
    (eval-in-emacs
     (if (search "youtu" url)
         `(progn (youtube-dl ,url) (youtube-dl-list))
         `(ambrevar/youtube-dl-url ,url)))))
(define-key "C-M-c d" 'youtube-dl-current-page)

(define-command play-video-in-current-page ()
  "Play video in the currently open buffer."
  (with-result (url (buffer-get-url))
    (uiop:run-program (list "mpv" url))))
(define-key "C-M-c v" 'play-video-in-current-page)
