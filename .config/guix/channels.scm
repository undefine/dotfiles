(cons*
 (channel
  (name 'nonguix)
  (url "https://gitlab.com/nonguix/nonguix.git")
  (branch "master"))
 (channel
  (name 'guix-gaming-games)
  (url "https://gitlab.com/guix-gaming-channels/games.git")
  (branch "master"))
 (channel
  (name 'guix-gaming-duke-nukem-3d)
  (url "https://gitlab.com/guix-gaming-channels/duke-nukem-3d.git")
  (branch "master"))
 (channel
  (name 'guix-gaming-quake-3)
  (url "https://gitlab.com/guix-gaming-channels/quake-3.git")
  (branch "master"))
 %default-channels)
