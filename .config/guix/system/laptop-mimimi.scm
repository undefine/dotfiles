;; With Eshell:
;; *sudo -E guix system -L ~/.config/guix/system reconfigure ~/.config/guix/system/laptop-mimimi.scm
(define-module (laptop-mimimi)
  #:use-module (default)
  #:use-module (nongnu packages linux)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu bootloader grub)
  #:use-module (gnu bootloader)
  #:use-module (gnu packages linux)
  #:use-module (gnu system mapped-devices)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system)
  #:use-module (guix build-system trivial)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (srfi srfi-1))

(define linux-xiaomi-air-13
  (package
    (inherit linux-libre)
    (name "linux-xiaomi-air-13")
    (version "5.0.7")
    ;; To follow linux-libre version, we can use the following, but this will
    ;; break if we don't immediately update the checksum.  (version
    ;; (package-version linux-libre))
    (source
     (origin
      (method url-fetch)
      (uri
       (string-append
        "https://www.kernel.org/pub/linux/kernel/v"
        (version-major version)
        ".x/linux-" version ".tar.xz"))
      (sha256
       (base32
        "1v2lxwamnfm879a9qi9fwp5zyvlzjw9qa0aizidjbiwz5dk7gq8n"))))
    (home-page "https://www.kernel.org/")
    (synopsis "Vanilla Linux kernel")
    (description "Vanilla Linux kernel which allows for non-free kernel modules / firmware.")
    ;; To build a custom kernel, pass it an alternate "kconfig":
    (native-inputs
     `(("kconfig" ,(local-file "../linux-laptop.conf"))
       ,@(alist-delete "kconfig" (package-native-inputs linux-libre))))))

(operating-system
  (inherit default-operating-system)
  (kernel linux-xiaomi-air-13)

  ;; The UUID is that returned by 'cryptsetup luksUUID'.
  (mapped-devices
   (list (mapped-device
          (source (uuid "b29cb68b-b154-4228-a131-34e9c474b0bd"))
          (target "guix")
          (type luks-device-mapping))))

  (file-systems (cons* (file-system
                         (device (file-system-label "guix"))
                         (mount-point "/")
                         (type "ext4")
                         (dependencies mapped-devices))
                       (file-system
                         (device (uuid "4E30-891F" 'fat))
                         (mount-point "/boot/efi")
                         (type "vfat"))
                       (file-system
                         (mount-point "/tmp")
                         (device "none")
                         (type "tmpfs")
                         (check? #f))
                       %base-file-systems)))
