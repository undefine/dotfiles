;; With Eshell:
;; *sudo -E guix system -L ~/.config/guix/system reconfigure ~/.config/guix/system/desktop-bababa.scm
(define-module (desktop-bababa)
  #:use-module (default)
  #:use-module (nongnu packages linux)
  #:use-module (gnu system)
  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader grub)
  #:use-module (gnu packages linux)
  #:use-module (gnu system file-systems))

(operating-system
  (inherit default-operating-system)
  (host-name "bababa")

  (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (timeout 1)
               (target "/dev/sdb")))

  ;; (kernel linux-libre)
  ;; (firmware %base-firmware)
  (firmware (append (list amdgpu-firmware)
                    %base-firmware))

  (file-systems (cons* (file-system
                         (device (file-system-label "guixsd"))
                         (mount-point "/")
                         (type "ext4"))
                       (file-system
                         (device (file-system-label "babadata"))
                         (mount-point "/media/babadata")
                         (type "btrfs")
                         (create-mount-point? #t))
                       (file-system
                         (mount-point "/tmp")
                         (device "none")
                         (type "tmpfs")
                         (check? #f))
                       %base-file-systems)))
