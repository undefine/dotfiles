;; Run as ‘guix pull -C FILE’.

(cons*
 (channel
  (name 'nonguix)
  (url "https://gitlab.com/nonguix/nonguix.git")
  (branch "master"))
 (channel
  (name 'guix-gaming-games)
  (url "https://gitlab.com/guix-gaming-channels/games.git")
  (branch "master"))
 (channel
  (name 'guix-gaming-duke-nukem-3d)
  (url "https://gitlab.com/guix-gaming-channels/duke-nukem-3d.git")
  (branch "master"))
 (channel
  (name 'guix-gaming-quake-3)
  (url "https://gitlab.com/guix-gaming-channels/quake-3.git")
  (branch "master"))
 (list
  (channel
   ;; This channel can be used as an alternative to
   ;; %default-channels to avoid rebuilding Guix on =guix pull=.
   (inherit (car %default-channels))
   ;; Get commit from =guix describe -f channels=.
   (commit "338defe0cbe156e8416647035403fccf059555e0"))))
