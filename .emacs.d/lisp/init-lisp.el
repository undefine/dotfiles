;;; Lisp

(require 'init-lispy)
(require 'init-rainbow)

(defun ambrevar/enhance-imenu-lisp (&rest keywords)
  "Add define-KEYWORD to `lisp-imenu-generic-expression'."
  (dolist (keyword keywords)
    (add-to-list
     'lisp-imenu-generic-expression
     (list (purecopy (concat (capitalize keyword) "s"))
           (purecopy (concat "^\\s-*("
                             (eval-when-compile
                               (regexp-opt
                                (list (concat "define-" keyword))
                                t))
                             "\\s-+\\(" lisp-mode-symbol-regexp "\\)"))
           2))))
;; Following defines are common, e.g. in Next.
(ambrevar/enhance-imenu-lisp "mode" "command" "parenscript")

;; For cl-dbus.
(add-to-list
 'lisp-imenu-generic-expression
 (list (purecopy "D-Bus")
       (purecopy (concat "^\\s-*("
                         (eval-when-compile
                           (regexp-opt
                            (list "dbus:define-dbus-method")
                            t))
                         "\\s-+(\\([^)]+\\)"))
       2))

(add-hook 'lisp-mode-hook 'ambrevar/turn-on-prettify-before-save)
(add-hook 'lisp-mode-hook 'ambrevar/turn-on-complete-filename)
(add-hook 'lisp-mode-hook 'ambrevar/turn-on-tab-width-to-8) ; Because some existing code uses tabs.
(add-hook 'lisp-mode-hook 'ambrevar/turn-off-indent-tabs)   ; Should not use tabs.
(add-hook 'lisp-mode-hook 'ambrevar/init-lispy)
(when (fboundp 'rainbow-delimiters-mode)
  (add-hook 'lisp-mode-hook #'rainbow-delimiters-mode))

;;; Common LISP.
(defun ambrevar/slime-switch-to-repl () ; TODO: Replace with `helm-defswitch'.
  (interactive)
  (require 'slime)
  (pcase (length slime-net-processes)
    (0 (slime))
    (1 (if (and (eq (current-buffer) (slime-output-buffer))
                (require 'helm-slime nil 'no-error))
           (helm-slime-mini)
         (pop-to-buffer (slime-output-buffer))))
    (_ (if (require 'helm-slime nil 'noerror)
           (helm-slime-mini)
         (pop-to-buffer (slime-output-buffer))))))

(defun slime-rainbow-init ()
  (font-lock-mode -1)
  (rainbow-delimiters-mode)
  (font-lock-mode))

(with-eval-after-load 'slime
  ;; REVIEW: Fix issue https://github.com/slime/slime/issues/523:
  (setq slime-defpackage-regexp
        "^(\\(cl:\\|common-lisp:\\|uiop:\\)?\\(defpackage\\|define-package\\)\\>[ \t']*")

  (setq slime-lisp-implementations
        '((sbcl ("sbcl" "--noinform"))
          (ccl ("ccl"))
          (ecl ("ecl"))))
  (let ((slime-extra '(slime-fancy
                       ;; slime-banner
                       slime-xref-browser
                       ;; slime-highlight-edits ; A bit slow...
                       slime-sprof
                       slime-quicklisp
                       slime-asdf
                       slime-indentation)))
    ;; slime-company should not be required, see
    ;; https://github.com/anwyn/slime-company/issues/11.
    ;; TODO: Fix slime-repl-ansi-color.
    ;; (when (require 'slime-repl-ansi-color nil t)
    ;;   (add-to-list 'slime-extra 'slime-repl-ansi-color)
    ;;   (setq slime-repl-ansi-color t))
    (when (ignore-errors (find-library-name "slime-company"))
      (add-to-list 'slime-extra 'slime-company))
    (slime-setup slime-extra)
    (add-hook 'slime-repl-mode-hook 'ambrevar/init-lispy)
    (add-hook 'slime-repl-mode-hook #'slime-rainbow-init)))

(when (require 'helm-slime nil 'noerror)
  (global-helm-slime-mode)
  (add-to-list 'helm-source-names-using-follow "SLIME xrefs"))

;; Read CLHS locally.
(or
 ;; Quicklisp package.
 (load "~/.quicklisp/clhs-use-local.el" 'noerror)
 ;; Unofficial Guix package (non-free license).
 (when (require 'clhs nil 'noerror)
   (clhs-setup)))

(with-eval-after-load 'redshank
  (setq redshank-accessor-name-function 'redshank-accessor-name/%)
  (define-key lisp-mode-map (kbd "C-x C-r c") 'redshank-defclass-skeleton)
  (define-key lisp-mode-map (kbd "C-x C-r s") 'redshank-asdf-defsystem-skeleton))

(provide 'init-lisp)
